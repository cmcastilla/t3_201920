package model.logic;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.opencsv.CSVReader;
import java.util.*;
import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.logic.UBERTrip;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private IArregloDinamico datos;


	private static final String SAMPLE_CSV_FILE_PATH = "./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv";

	ArrayList<UBERTrip> uber = new ArrayList<UBERTrip>();





	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos = new ArregloDinamico(7);

	}

	public void cargar()
	{
		Reader reader;
		try {
			reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
			CSVReader csvReader = new CSVReader(reader);

			String[] nextRecord;
			csvReader.readNext();
			while ((nextRecord = csvReader.readNext()) != null) {
				double sourceid = Double.parseDouble(nextRecord[0]);
				double dstid = Double.parseDouble(nextRecord[1]);
				double hod = Double.parseDouble(nextRecord[2]);
				double mean_travel_time = Double.parseDouble(nextRecord[3]);
				double standard_deviation_travel_time = Double.parseDouble(nextRecord[4]);
				double geometric_mean_travel_time = Double.parseDouble(nextRecord[5]);
				double geometric_standard_deviation_travel_time = Double.parseDouble(nextRecord[6]);




				//demas atributos

				UBERTrip t = new UBERTrip( sourceid,   dstid,  hod,  mean_travel_time,  standard_deviation_travel_time,  geometric_mean_travel_time,  geometric_standard_deviation_travel_time);

				//pilaViajes.push(t);
				uber.add(t);
			}



		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Constructor del modelo del mundo con capacidad dada
	 * @param tamano
	 */
	public MVCModelo(int capacidad)
	{
		datos = new ArregloDinamico(capacidad);
	}

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return uber.size();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(String dato)
	{	
		datos.agregar(dato);
	}

	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public String buscar(String dato)
	{
		return datos.buscar(dato);
	}

	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public String eliminar(String dato)
	{
		return datos.eliminar(dato);
	}

	public void metodo1(){

	}


	public static void main(String[] args) {

		MVCModelo m = new MVCModelo();
	}

	// metodos 

	public ArrayList<UBERTrip> listaViajesDadoUnaHora(int a) {
		ArrayList<UBERTrip> temporal = new ArrayList<UBERTrip>();

		for (int i = 0; i < uber.size(); i++) {
			if (uber.get(i).getHod() == a) {
				temporal.add(uber.get(i));
			}
		}
		return temporal;

	}
	public static void mergeSort(int[] a, int n) {
		if (n < 2) {
			return;
		}
		int mid = n / 2;
		int[] l = new int[mid];
		int[] r = new int[n - mid];

		for (int i = 0; i < mid; i++) {
			l[i] = a[i];
		}
		for (int i = mid; i < n; i++) {
			r[i - mid] = a[i];
		}
		mergeSort(l, mid);
		mergeSort(r, n - mid);

		merge(a, l, r, mid, n - mid);
	}
	public static void merge(
			int[] a, int[] l, int[] r, int left, int right) {

		int i = 0, j = 0, k = 0;
		while (i < left && j < right) {
			if (l[i] <= r[j]) {
				a[k++] = l[i++];
			}
			else {
				a[k++] = r[j++];
			}
		}
		while (i < left) {
			a[k++] = l[i++];
		}
		while (j < right) {
			a[k++] = r[j++];
		}
	}

	public static void sort(int arrayToSort[]) {
		int n = arrayToSort.length;

		for (int gap = n / 2; gap > 0; gap /= 2) {
			for (int i = gap; i < n; i++) {
				int key = arrayToSort[i];
				int j = i;
				while (j >= gap && arrayToSort[j - gap] > key) {
					arrayToSort[j] = arrayToSort[j - gap];
					j -= gap;
				}
				arrayToSort[j] = key;
			}
		}
	}
	public static void quickSort(int arr[], int begin, int end)
	{
		if (begin < end) {
			int partitionIndex = partition(arr, begin, end);

			// Recursively sort elements of the 2 sub-arrays
			quickSort(arr, begin, partitionIndex-1);
			quickSort(arr, partitionIndex+1, end);
		}
	}

	private static int partition(int arr[], int begin, int end)
	{
		int pivot = arr[end];
		int i = (begin-1);

		for (int j=begin; j<end; j++)
		{
			if (arr[j] <= pivot) {
				i++;

				int swapTemp = arr[i];
				arr[i] = arr[j];
				arr[j] = swapTemp;
			}
		}

		int swapTemp = arr[i+1];
		arr[i+1] = arr[end];
		arr[end] = swapTemp;

		return i+1;
	}



}
